#include <algorithm>
#include <atomic>
#include <cassert>
#include <chrono>
#include <condition_variable>
#include <fstream>
#include <iostream>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

#ifdef __cpp_lib_hardware_interference_size
    using std::hardware_constructive_interference_size;
    using std::hardware_destructive_interference_size;
#else
    constexpr std::size_t hardware_constructive_interference_size = 64;
    constexpr std::size_t hardware_destructive_interference_size = 64;
#endif

constexpr int max_iterations = 1000000L;

inline uint64_t
rdtsc() {
	unsigned int lo, hi;
	asm volatile ( "rdtsc\n" : "=a" (lo), "=d" (hi) );

	return ((uint64_t)hi << 32) | lo;
}

size_t
getDataCacheLineSize() {

    std::ifstream ifs("/sys/devices/system/cpu/cpu0/cache/index0/coherency_line_size", std::ifstream::in | std::ifstream::binary);

    if (ifs.is_open()) {
        size_t sz;
        ifs >> sz;
        return sz;
    }

    return 0;
}

inline auto
now() noexcept { return std::chrono::high_resolution_clock::now(); }


alignas(hardware_constructive_interference_size) std::atomic_int seq_count_1 = {-1};
alignas(hardware_constructive_interference_size) std::atomic_int seq_count_2 = {-1};

volatile uint64_t rdtsc_tick{};

std::vector<uint64_t> results(max_iterations);
 
std::mutex mtx;
std::condition_variable cv;
bool start = false;

template<bool odd>
void workerThread() {

    std::this_thread::sleep_for(std::chrono::milliseconds(20));
    //wait start
    {
        std::unique_lock<std::mutex> lock(mtx);
        cv.wait(lock, []{return start;});
    }

    for (auto count = 0; count != max_iterations; ++count) {
        if constexpr (odd) {
            while (seq_count_1.load(std::memory_order_acquire) != count) ;

            rdtsc_tick = rdtsc();
            seq_count_2.store(count, std::memory_order_release);
        }
        else {
            seq_count_1.store(count, std::memory_order_release);

            while (seq_count_2.load(std::memory_order_acquire) != count) ;
            results[count] = rdtsc() - rdtsc_tick;
        }
    }
}

int main()
{
    auto dataCacheLineSize = hardware_constructive_interference_size;

    std::cout << "Data cache line size: " << dataCacheLineSize << std::endl;
    if (0 == dataCacheLineSize) {
        std::cerr << "Cannot get data cache size !\n";
        return -1;
    }

    auto num_cores = std::thread::hardware_concurrency();
    if (num_cores < 2) {
        std::cerr << "Not enough cores !\n";
        return -2;
    }

    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(0, &cpuset);
    int rc = pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpuset);
    if (rc != 0) {
        std::cerr << "Error calling pthread_setaffinity_np: " << rc << "\n";
        return -3;
    }

    auto cycles_start = rdtsc();
    auto time_start = now();

    std::this_thread::sleep_for(std::chrono::milliseconds(100));

    auto elapsed_cycles = rdtsc() - cycles_start;
    const std::chrono::duration<double, std::ratio<1, 1>> elapsed_time { now() - time_start };
    auto cycles_per_time = elapsed_cycles / elapsed_time.count();

    std::cout << "Approx CPU MHz: " << cycles_per_time / 1000000.0 << std::endl;
    
    std::thread thread1{ workerThread<0> };
    CPU_SET(1, &cpuset);
    rc = pthread_setaffinity_np(thread1.native_handle(), sizeof(cpu_set_t), &cpuset);
    if (rc != 0) {
        std::cerr << "Error calling pthread_setaffinity_np: " << rc << "\n";
        return -3;
    }

    std::thread thread2{ workerThread<1> };
    CPU_SET(2, &cpuset);
    rc = pthread_setaffinity_np(thread2.native_handle(), sizeof(cpu_set_t), &cpuset);
    if (rc != 0) {
        std::cerr << "Error calling pthread_setaffinity_np: " << rc << "\n";
        return -3;
    }

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    {
        std::lock_guard<std::mutex> lock(mtx);
        start = true;
        cv.notify_all();
    }

    thread1.join();
    thread2.join();

    std::vector<uint64_t>::iterator middle = results.begin() + (results.end() - results.begin()) / 2;
    std::nth_element(results.begin(), middle, results.end());

    std::cout << "Cache Latency Median Time: " << (*middle / cycles_per_time * 1000000000.0) << " ns\n";

    return 0;
}