CXX = g++
CXX_FLAGS = -ffunction-sections -fdata-sections -pthread -Wall -Wextra -O3 -DNDEBUG -fPIC -fstack-protector-strong -std=c++17

ODIR=./
LDIR =

LIBS=

_DEPS =
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

_OBJ = cachelatency.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))


$(ODIR)/%.o: %.cpp $(DEPS)
	$(CXX) -c -o $@ $< $(CXX_FLAGS)

cachelatency: $(OBJ)
	$(CXX) -o $@ $^ $(CXX_FLAGS) $(LIBS)

.PHONY: clean

clean:
	rm -f $(ODIR)/*.o *~ core $(INCDIR)/*~